#!/bin/sh

Year=$(date "+%Y")
if [ -z "$Year" ] ; then
	echo "Invalid year returned by date command: $Year"
	exit 1
fi

perl -p -i -e "s/(^ \*\s{50,}\d{4},) 20\d{2}/\1 $Year/" *.c

#!/bin/sh

Sources="."
Target=".."

# check version number in every file
Version=$(sed -n -e 's/^SET(sarg_VERSION \([0-9]\))/\1/p' "$Sources/CMakeLists.txt")
if [ -z "$Version" ] ; then
	echo "No version found in CMakeLists.txt"
	exit 1
fi
Revision=$(sed -n -e 's/^SET(sarg_REVISION "\([0-9][0-9]*.*\)")/\1/p' "$Sources/CMakeLists.txt")
if [ -z "$Revision" ] ; then
	echo "No revision found in CMakeLists.txt"
	exit 1
fi
Version="$Version.$Revision"

VersionCheck=$(sed -n -e 's/^AC_INIT(\[sarg\],\[\([0-9][0-9]*\.[0-9][0-9]*.*\)\])/\1/p' "$Sources/configure.in")
if [ -z "$VersionCheck" ] ; then
	echo "No version found in configure.in"
	exit 1
fi
if [ "x$Version" != "x$VersionCheck" ] ; then
	echo "CMakeLists and configure doesn't refer to the same version"
	exit 1
fi

if [ ! -f "$Sources/configure" ] ; then
	echo "Configure script not created"
	exit 1
fi

ConfigureCheck=$(sed -n -e "s/^PACKAGE_VERSION='\([0-9][0-9]*\.[0-9][0-9]*.*\)'/\1/p" "$Sources/configure")
if [ "x$Version" != "x$ConfigureCheck" ] ; then
	echo "Configure script not updated"
	echo "Run autoreconf"
	exit 1
fi

# Check release date
ReleaseDate=$(sed -n -e 's/^SET(sarg_BUILDDATE "\([A-Z][a-z][a-z]-[0-3][0-9]-2[0-1][0-9][0-9]\)")/\1/p' "$Sources/CMakeLists.txt")
if [ -z "$ReleaseDate" ] ; then
	echo "No release date found or release date is invalide in CMakeLists.txt"
	exit 1
fi

ReleaseDateCheck=$(sed -n -e 's/^#define VERSION PACKAGE_VERSION" \([A-Z][a-z][a-z]-[0-3][0-9]-2[0-1][0-9][0-9]\)"/\1/p' "$Sources/include/info.h")
if [ -z "$ReleaseDateCheck" ] ; then
	echo "No release date found or release date is invalide in include/info.h"
	exit 1
fi
if [ "x$ReleaseDate" != "x$ReleaseDateCheck" ] ; then
	echo "CMakeLists and include/info.h doesn't refer to the same release date"
	exit 1
fi

# make sure the ChangeLog has been updated
LogEntry=$(sed -n -e "1,5{;/^$ReleaseDate Version $Version$/p;}" $Sources/ChangeLog)
if [ -z "$LogEntry" ] ; then
	echo "ChangeLog not up to date"
	echo "It should contain an entry for \"$ReleaseDate Version $Version\""
	exit 1
fi

# update the po files
if ( ! make update-po ) ; then
	echo "Failed to update the po files"
	exit 1
fi

# is the git repository dirty?
if [[ $(git diff --shortstat 2> /dev/null) != "" ]] ; then
	echo "Git repository is dirty"
	exit 1
fi

# check the working tree
if ( ! git diff-files --quiet ) ; then
	echo "Uncommitted changes in working tree"
	exit 1
fi

# check the index for uncommitted changes
if ( ! git diff-index --quiet --cached HEAD ) ; then
	echo "Uncommitted changes in the index"
	exit 1
fi

# check for untracked files
Untracked="$(git ls-files --exclude-standard --others)"
if [[ -n "$Untracked" ]] ; then
	echo "Untracked files in directory"
	echo "$Untracked"
	exit 1
fi

# is the git tag set?
# a valid tag can be v2.3.3 or v2.3.3_da to indicate version 2.3.3 with the Danish translation
GitVersion=$(git describe | sed -e 's/^\(v[0-9][0-9.]*\)_.*/\1/')
if [[ "$GitVersion" != "v$Version" ]] ; then
	echo "Git tag not set for version $Version. Use command:"
	echo "git tag -a \"v$Version\" -m \"v$Version\""
	exit 1
fi

# prepare the archive
SargList="$Target/sarglist.txt.unsort"
SortList="$Target/sarglist.txt"
DirList="$Target/sargdir.txt"
ExcludeList="$Target/sargexclude.txt"
ArchiveFile="$Target/sarg-$Version.tar"

SedSources=$(echo "$Sources" | sed -e 's/\./\\./')
ls $Sources/*.c | sed -e "s/^$SedSources\///" > $SargList
ls $Sources/include/*.h | sed -e "s/^$SedSources\///" >> $SargList
ls $Sources/po/*.{po,gmo,pot} | sed -e "s/^$SedSources\///" >> $SargList

echo "
config.h
ABOUT-NLS
aclocal.m4
BETA-TESTERS
ChangeLog
CMakeLists.txt
configure.in
CONTRIBUTORS
COPYING
css.tpl
DONATIONS
Doxyfile
exclude_codes
htaccess
LICENSE
Makefile.in
PROGRAMMERS
README
README_cmake
sarg.1
sarg.conf
sarg_htaccess
user_limit_block
cfgaux
documentation
fonts
images
include/config.h.in
sarg-php
sarg_manpage.xml
po/remove-potcdate.sin
po/Makefile.in.in
po/POTFILES.in
po/quot.sed
po/Rules-quot
po/insert-header.sin
po/ChangeLog
po/LINGUAS
po/en@quot.header
po/en@boldquot.header
po/boldquot.sed
po/Makevars
sorttable/sorttable.js
" >> $SargList

[ -f "$DirList" ] && rm "$DirList"
cat $SargList | while read file
do
	[ -z "$file" ] && continue
	if [ -f "$file" ] ; then
		echo "$file" >> "$DirList"
	elif [ -d "$file" ] ; then
		find "$file" -type f >> "$DirList"
	else
		echo "Unknown file type $file"
		exit 1
	fi
done
rm "$SargList"

sort -u "$DirList" | sed -e '/^$/d' > "$SortList"
rm "$DirList"

echo "
*/.svn
*/.git
*~
*.o
.gitignore
" > "$ExcludeList"

tar cf "$ArchiveFile" -C "$Sources" --files-from="$SortList" --exclude-from="$ExcludeList" --no-recursion --transform="s,^,sarg-$Version/," --owner root --group root --mode 644
tar rf "$ArchiveFile" -C "$Sources" "configure" --exclude-from="$ExcludeList" --transform="s,^,sarg-$Version/," --owner root --group root --mode 755
gzip -f "$ArchiveFile"
CompressFile="$ArchiveFile.gz"

md5sum $CompressFile > "$Target/sarg-$Version.md5sum"
echo "md5sum:"
cat "$Target/sarg-$Version.md5sum"

sha1sum $CompressFile > "$Target/sarg-$Version.sha1sum"
echo "sha1sum:"
cat "$Target/sarg-$Version.sha1sum"

sha256sum $CompressFile > "$Target/sarg-$Version.sha256sum"
echo "sha256sum:"
cat "$Target/sarg-$Version.sha256sum"
